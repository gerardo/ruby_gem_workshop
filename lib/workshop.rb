# frozen_string_literal: true

require_relative "workshop/version"

module Workshop
  class Error < StandardError; end
  # Your code goes here...
  def self.hi
    "Hello!"
  end
end
