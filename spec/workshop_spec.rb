# frozen_string_literal: true

RSpec.describe Workshop do
  it "has a version number" do
    expect(Workshop::VERSION).not_to be nil
  end

  it "says hi " do
    expect(Workhshop.hi).to eq("Hello!")
  end
end
